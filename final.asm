  .data
dimension: .word 2
filename_a: .asciiz "mat_a.dat"
filename_b: .asciiz "mat_b.dat"
filename_c: .asciiz "mat_c.dat"
filename_out: .asciiz "mat_out.dat"

  .text
main:
  # calculate bytes in the files based on dimension
  # bytes = 4(dim * dim)
  lw $t0, dimension
  mul $t0, $t0, $t0
  mul $s0, $t0, 4

  # load matricies from files
  la $a0, filename_a
  move $a1, $s0
  jal load
  move $s1, $v0

  la $a0, filename_b
  move $a1, $s0
  jal load
  move $s2, $v0

  la $a0, filename_c
  move $a1, $s0
  jal load
  move $s3, $v0

  # multiply the first two
  move $a0, $s1
  move $a1, $s2
  lw $a2, dimension
  jal matrix_mul
  move $s4, $v0

  # add the result and the third matrix
  move $a0, $s3
  move $a1, $s4
  move $a2, $s0
  jal matrix_add
  move $s5, $v0

  # save the result to file
  la $a0, filename_out
  move $a1, $s5
  move $a2, $s0
  jal write

  li $v0, 10
  syscall

########
# math
########

# square matrix multiplication
# a0 - address of first matrix
# a1 - address of second matrix
# a2 - matrix dimensions
# returns:
# v0 - address of result matrix
matrix_mul:
  # save registers $s0-$s3
  add $sp, $sp, -16
  sw $s0, 12($sp)
  sw $s1, 8($sp)
  sw $s2, 4($sp)
  sw $s3, ($sp)

  move $s1, $a0 # matrix a
  move $s2, $a1 # matrix b
  move $s3, $a2 # dimensions

  # calculate space needed for result
  # and request the space from the os
  mul $t0, $a2, $a2 # number of floats needed
  mul $t1, $t0, 4   # number of bytes needed
  move $a0, $t1
  li $v0, 9
  syscall
  sub $v0, $v0, $t1 # set the pointer to the 'start' of the block
  move $s0, $v0     # we'll return this address

  # t0 - counting rows in mat_a
  # t1 - counting cols in mat_b
  # t2 - counting current item in running sum
  # t3 - address for current item in mat_a
  # t4 - address for current item in mat_b
  # t5 - address for current item in mat_c
  # s0 - address for mat_c
  # s1 - address for mat_a
  # s2 - address for mat_b
  # s3 - matrix dimensions
  # f0 - accumulator for running sum
  # f1 - holds float from mat_a
  # f2 - holds float from mat_b
  li $t0, 0
  li $t1, 0
  li $t2, 0
  move $t3, $s1
  move $t4, $s2
  move $t5, $s0
  mtc1 $0, $f0
  # for every row in matrix a
mm_each_row_in_a:
  li $t1, 0
  # for every column in matrix b
mm_each_col_in_b:
  mtc1 $0, $f0
  li $t2, 0
  # for every pair, compute running sum
mm_compute_cell:
  # calculate address of double in matrix a
  # t3 = base + 4((t0 * dim) + t2)
  mul $t6, $t0, $s3
  add $t6, $t6, $t2
  mul $t6, $t6, 4
  add $t3, $s1, $t6

  # calculate address of double in matrix b
  # t4 = base + 4(t1 + (t2 * dim))
  mul $t6, $t2, $s3
  add $t6, $t6, $t1
  mul $t6, $t6, 4
  add $t4, $s2, $t6

  # load the floats
  l.s $f1, ($t3)
  l.s $f2, ($t4)

  mul.s $f3, $f1, $f2  # multiply the two floats together
  add.s $f0, $f0, $f3  # add result to the running sum

  addi $t2, $t2, 1
  blt $t2, $s3, mm_compute_cell

  # calculate the address to store
  # t5 = base + 4((t0 * dim) + t1)
  mul $t6, $t0, $s3
  add $t6, $t6, $t1
  mul $t6, $t6, 4
  add $t5, $t6, $s0
  s.s $f0, ($t5)       # save the resulting sum to the result matrix

  # go to the next column in mat_b
  addi $t1, $t1, 1     # next column in mat_b
  blt $t1, $s3, mm_each_col_in_b

  # go to the next row in mat_a
  addi $t0, $t0, 1
  blt $t0, $s3, mm_each_row_in_a

  # return the address of the new matrix
  move $v0, $s0
  # restore $s0-$s3, stack pointer
  lw $s0, 12($sp)
  lw $s1, 8($sp)
  lw $s2, 4($sp)
  lw $s3, ($sp)
  add $sp, $sp, 16
  jr $ra

# square matrix addition
# a0 - address of matrix a in data segment
# a1 - address of matrix b in data segment
# a2 - matrix dimension
# returns
# v0 - address of solution matrix
matrix_add:
  
  # set up loop with matrix addresses
  move $t0, $a2 # dimensions
  move $t1, $a0 # matrix a
  move $t2, $a1 # matrix b

  # request space for new matrix
  mul $a0, $t0, $t0
  li $v0, 9
  syscall
  sub $v0, $v0, $a0 # set the reference to the 'start' of the block
  move $t3, $v0
  # move $t3, $v0 # copy for looping through
  # move $v0, $v0 # copy for returning
  
ma_loop:
  # load values from source matricies
  l.s $f0, ($t1)
  l.s $f2, ($t2)

  # add and store
  add.s $f4, $f2, $f0
  s.s $f4, ($t3)

  # increment everything
  addi $t0, $t0, -1
  addi $t1, $t1, 4
  addi $t2, $t2, 4
  addi $t3, $t3, 4

  # check if we're done
  bgez $t0, ma_loop

  # make sure we return address of new matrix
  # no procedure calls made, $v0 still has proper value
  # move $v0, $s0
	jr $ra


########
# file io
########

# load bytes from a file into a buffer
# a0 - filename
# a1 - bytes to read
# returns
# v0 - address of buffer with the read bytes
load:
  # preserve registers $s0-$s3
  add $sp, $sp, -16
  sw $s0, 12($sp)
  sw $s1, 8($sp)
  sw $s2, 4($sp)
  sw $s3, ($sp)

	# move arguments to registers
	move $s0, $a0
	move $s1, $a1

	# get the file descriptor
	# a0 - filename
	# a1 - flags: 0 -> O_RDONLY
	# a2 - mode
	li $v0, 13
	move $a0, $s0
	li $a1, 0
	li $a2, 0
	syscall
	move $s2, $v0

  # get some memory to read into
  move $a0, $s1
  li $v0, 9
  syscall
  sub $v0, $v0, $s1 # set pointer to 'start' of buffer
  move $s3, $v0

	# read the file into the buffer
	# a0 - file descriptor
	# a1 - buffer
	# a2 - bytes to read
	li $v0, 14
	move $a0, $s2
	move $a1, $s3
	move $a2, $s1
	syscall

	# close the file
	# a0 - file descriptor
	li $v0, 16
	move $a0, $s2
	syscall

  # return the address of the new buffer
  move $v0, $s3

  # restore registers $s0-$s3
  lw $s0, 12($sp)
  lw $s1, 8($sp)
  lw $s2, 4($sp)
  lw $s3, ($sp)
  add $sp, $sp, 16

	jr $ra


# write bytes to a file
# a0 - filename
# a1 - buffer
# a2 - bytes to write
write:
  # preserve registers $s0-$s3
  add $sp, $sp, -16
  sw $s0, 12($sp)
  sw $s1, 8($sp)
  sw $s2, 4($sp)
  sw $s3, ($sp)

	move $s0, $a0
	move $s1, $a1
	move $s2, $a2
	# open a file for writing
	# a0 - filename
	# a1 - flags: 2 -> O_RDWR
  #   O_RDWR   - 0b0000_0000_0010 - read/write
  #   O_APPEND - 0b0000_0000_1000 - append
  #   O_CREAT  - 0b0010_0000_0000 - create if non-existant
  #   O_TRUNC  - 0b0100_0000_0000 - truncate to 0 if it exists
  #   bitwise |->0b0110_0000_1010 - 0x60a 
	# a2 - mode
  #   0x1FF    - 0x0001_1111_1111 - permissions for everyone
	li $v0, 13
	move $a0, $s0
	la $a1, 0x60a
  li $a2, 0x1FF
	syscall
	move $s3, $v0

	# write the bytes from the buffer into the file
	# buffer is in $s1
	# bytes to write is in $s2
	li $v0, 15
	move $a0, $s3
	move $a1, $s1
	move $a2, $s2
	syscall

  # restore registers $s0-$s3
  lw $s0, 12($sp)
  lw $s1, 8($sp)
  lw $s2, 4($sp)
  lw $s3, ($sp)
  add $sp, $sp, 16
	jr $ra
