#include <stdio.h>
#include <fcntl.h>

int main (int argc, char *argv[]) {
	float matrixa[16] = {
    0.0,  1.0,  2.0,  3.0,
    4.0,  5.0,  6.0,  7.0,
    8.0,  9.0,  10.0, 11.0,
    12.0, 13.0, 14.0, 15.0};
	FILE *fd = fopen("mat_a.dat", "w+");
	fwrite(matrixa, 4, 16, fd);
	float matrixb[16] = {
   .00, .10, .20, .30,
   .40, .50, .60, .70,
   .80, .90, 1.0, 1.1,
   1.2, 1.3, 1.4, 1.5};
	fd = fopen("mat_b.dat", "w+");
	fwrite(matrixb, 4, 16, fd);
	float matrixc[16] = {
    100.0, 101.0, 102.0, 103.0,
    104.0, 105.0, 106.0, 107.0,
    108.0, 109.0, 110.0, 111.0,
    112.0, 113.0, 114.0, 115.0};
	fd = fopen("mat_c.dat", "w+");
	fwrite(matrixc, 4, 16, fd);
	return 0;
}
