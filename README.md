mips matrix arithmetic
======================

This program performs the operation `A * B + C` using single-precision floating-point arithmetic where `A`, `B`, and `C` are _n_ x _n_ square matricies. It reads input from and writes output to files.

## files included

### `final.asm`

This is the mips assembly code that performs the matrix operations. The file is long because it does a lot. Here's the layout of the file:

```
  * data segment
  * main routine
  * math
   * matrix_mul
   * matrix_add
  * file io
   * load
   * write
```

Each function has comments that detail their inputs and outputs. The code performs the operation: `mat_a.dat` * `mat_b.dat` + `mat_c.dat` => `mat_out.dat`, where matrix data files `a`, `b`, and `c` are generated beforehand.

The routine takes one parameter, which is the dimension of the matrices it will accept. It is defined on line 2, in the data segment. Setting it to 2 will result in the program looking for 2x2 matrices. Setting it to 4 leads it to 4x4, and so on.

### `sml_matrix.c`

A C program that generates the binary `gen_sml_matrices`. If you want to change the small matrices generated, do so here.

### `med_matrix.c`

Same as `sml_matrix.c`, but generates the binary for medium-sized matrices.
