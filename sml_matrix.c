#include <stdio.h>
#include <fcntl.h>

int main (int argc, char *argv[]) {
	float matrixa[4] = { 0.0,  1.0,  2.0,  3.0};
	FILE *fd = fopen("mat_a.dat", "w+");
	fwrite(matrixa, 4, 4, fd);

	float matrixb[4] = { 4.0,  5.0,  6.0,  7.0};
	fd = fopen("mat_b.dat", "w+");
	fwrite(matrixb, 4, 4, fd);

	float matrixc[4] = { 1000.0,  1001.0,  1002.0,  1003.0};
	fd = fopen("mat_c.dat", "w+");
	fwrite(matrixc, 4, 4, fd);

	return 0;
}
